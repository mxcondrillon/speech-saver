<?php
	class Speech extends Database {

		function __construct()
		{
			parent::__construct();
		}

		public function add_speech()
		{


			$data = array(
				'author' => $_POST['author'], 
				'date' => $_POST['datetimepickeradd'], 
				'keywords' => str_replace(',','+',$_POST['keywords-add']), 
				'content' => $_POST['speechInput'], 
			);

			$function = array(
					'action' => 'insert',
					'table_name' => 'fs_speech' 
				);

			$result = $this->query($data,$function);

			if($result)
			{
				echo true;
			}else
			{
				echo false;
			}
		}

		public function get_speech()
		{
			if(isset($_POST['speech_id']))
				$query = "SELECT * FROM fs_speech WHERE id=".$_POST['speech_id'];
			else
				$query = "SELECT * FROM fs_speech";

			$result = $this->run_query($query);

			echo json_encode($result);
		}

		public function edit_speech()
		{
			
			$query = "UPDATE fs_speech SET author='".$_POST['view-speech-author']."',date='".$_POST['datetimepicker1']."',keywords='".str_replace(',','+',$_POST['keywords'])."',content='".$_POST['view-speech-content']."' WHERE id=".$_POST['view-speech-id'];

			$result = $this->update_query($query);

			echo json_encode($result);

		}

		public function delete_speech()
		{
			$query = "DELETE FROM fs_speech WHERE id=".$_POST['view-speech-id'];
			$result = $this->update_query($query);

			echo json_encode($result);
		}

	}
?>