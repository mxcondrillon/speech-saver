<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Speech Application</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.css" />
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- <script src="../../assets/js/ie-emulation-modes-warning.js"></script> -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  <div class="body-content"> 
    <nav class="navbar navbar-fixed-top">
      <div class="container-fluid hidden-xs">
        <div class="navbar-header btnbox">
          <div class="btn-group btn-group-justified" role="group" aria-label="...">
					  <div class="btn-group" role="group">
					    <button id='tab-view' type="button" data-target="#viewSpeech" class="btn btn-tab btn-default btn-primary">View my Speeches</button>
					  </div>
					  <div class="btn-group" role="group">
					    <button id="tab-submit" type="button" data-target="#submitSpeech" class="btn btn-tab btn-default">Submit a new Speech</button>
					  </div>
					  <div class="btn-group" role="group">
					    <button id="tab-search" type="button" data-target="#searchSpeech" class="btn btn-tab btn-default">Search all Speeches</button>
					  </div>
					</div>
        </div>
      </div>
      <div class="container hidden-sm hidden-md hidden-lg">
      	<nav class="navbar navbar-default">
				  <div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand showList" href="javascript:void(0);"><i class="fa fa-list-ul"></i> Search 1</a>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li class="active"><button id="mobile-tab-view" type="button" data-target="#viewSpeech" class="btn btn-tab btn-default btn-primary">View my Speeches</button></li>
				        <li><button id="mobile-tab-submit" type="button" data-target="#submitSpeech" class="btn btn-tab btn-default">Submit a new Speech</button></li>
				        <li><button id="mobile-tab-search" type="button" data-target="#searchSpeech" class="btn btn-tab btn-default">Search all Speeches</button></li>
				      </ul> 
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar slide-panel">
          <div class="listtitle hidden-sm hidden-md hidden-lg"> Back</div>
          <button class="hidepanel hidden-sm hidden-md hidden-lg"><i class="fa fa-angle-left"></i></button>
          <ul class="nav nav-sidebar" id="speech_list">
            <!-- <li class="active"><a href="#">Speech 1 <i class="showbox fa fa-angle-right"></i></a></li>
            <li><a href="#">Speech 2<i class="showbox fa fa-angle-right"></i></a></li>
            <li><a href="#">Speech 3<i class="showbox fa fa-angle-right"></i></a></li>
            <li><a href="#">Speech 4<i class="showbox fa fa-angle-right"></i></a></li>
            <li><a href="#">Speech 5<i class="showbox fa fa-angle-right"></i></a></li> -->
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <div id="viewSpeech" class="content-card show view-speeches">
            <div id="update-speech-response">
            </div>
          	<form id="speech-view-edit-delete-form" >
              <input type="hidden" id="view-speech-id" name="view-speech-id" />
						  <div class="form-group">
						    <textarea placeholder="Input Speech Here" class="form-control class-edit-func" rows="9" id="view-speech-content" name="view-speech-content" ></textarea>
						  </div>
					    <div class="form-group">
					      <div class="row full-width">
					        <div class="col-md-4 col-sm-4">
		                <div class='input-group'>
		                  <input placeholder="Author Name" type='text' class="form-control class-edit-func" id="view-speech-author" name="view-speech-author" />
		                </div>
                  </div>
                  <div class="col-md-4 col-sm-4">
		                <div class='input-group'>
		                 <input type="text" name="keywords" id="views-edit-delete-keywords" class="form-control class-edit-func"
                     value="" data-role="tagsinput" />
		                </div>
                  </div>
                  <div class="col-md-4 col-sm-4">
		                <div class='input-group date'>
		                  <input placeholder="Date" type='text' class="form-control class-edit-func" id='datetimepicker1' name='datetimepicker1' />
		                  <span class="input-group-addon">
		                    <span class="glyphicon glyphicon-calendar"></span>
		                  </span>
		                </div>
	                </div>
	              </div>  
              </div>
              <div class="float-right">
                <button type="button" class="btn btn-default btn-danger btn-function-if-select" id="btn-delete-speech" >Delete</button>
                <button type="button" class="btn btn-default btn-success btn-function-if-select" id="btn-edit-speech" >Save</button>
						    <button class="btn btn-default btn-info btn-function-if-select">Share</button>
						  </div>
						</form>
          </div>
          <div id="submitSpeech" class="content-card submit-speeches">
          	<form id="add-speech-form" >
          	  <div class="form-group">
					      <div class="row full-width">
					        <div class="col-md-12"><h1>Submit New Speech</h1></div>
                  <div class="col-md-12"  id="add-speech-response">
                   
                  </div>
					        <div class="col-md-4 col-sm-4">

		                <div class='input-group'>
		                  <label for="#author">Author Name:</label>
		                  <input name="author" id="author" placeholder="Author Name" type='text' class="form-control class-edit-func" />
		                </div>
                  </div>
                  <div class="col-md-4 col-sm-4">
		                <div class='input-group'>
		                   <label for="#speechInput">Keywords</label>
		                   <input value="" type="text" name="keywords-add" id="keywords-add" class="form-control class-edit-func" data-role="tagsinput" />
		                </div>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <label for="#datetimepickeradd">Date:</label>
		                <div class='input-group date'>
		                  <input placeholder="Date" type='text' class="form-control class-edit-func" id='datetimepickeradd' name='datetimepickeradd' />
		                  <span class="input-group-addon">
		                    <span class="glyphicon glyphicon-calendar"></span>
		                  </span>
		                </div>
	                </div>
	              </div>  
              </div>
						  <div class="form-group">
						    <label for="#speechInput">Speech Content:</label>
						    <textarea id="speechInput" name="speechInput" placeholder="Input Speech Here" class="form-control class-edit-func" rows="9"></textarea>
						  </div>
              <div class="float-right">
                <button type="button" class="btn btn-default btn-success" id="btn-add-speech-form" >Submit</button>
						    <button class="btn btn-default btn-primary">Cancel</button>
						  </div>
						</form>
          </div>
          <div id="searchSpeech" class="content-card submit-speeches">
          	<form>
          	  <div class="row">
          	    <div class="col-md-12">
				    		<h2>Custom search field</h2>
		            <div id="custom-search-input">
		                <div class="input-group col-md-12">
		                    <input type="text" class="form-control input-lg" placeholder="" />
		                    <span class="input-group-btn">
		                        <button class="btn btn-info btn-lg" type="button">
		                            <i class="glyphicon glyphicon-search"></i>
		                        </button>
		                    </span>
		                </div>
		            </div>
		            </div>
				        </div>
						</form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="fulloverlay">
     <div class="centerloader">
  	   <div class="loader"></div>
  	 </div>
  </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <!-- <script src="../../assets/js/vendor/holder.min.js"></script> -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
    <script>
    var cehck_if_edit = false;
	  $( function() {
	    $( "#datetimepicker1" ).datepicker();
	    $( "#datetimepickeradd" ).datepicker();
	  } );
    </script>
    <!--tab plugin-->
    <script>
    	$(document).ready(function(){
    		$('.btn-tab').click(function(){

          if(cehck_if_edit)
          {
            var con = confirm("Are you sure you want to leave this screen, any unsaved content will be lost.");
          }else
          {
            var con = true;
          }

          if(con)
          {
            cehck_if_edit = false;
      			resetTabs();
      			tabx = $(this).attr('data-target');
      			tabid = $(this).attr('id');
            if("tab-submit" == tabid){
              $("#add-speech-form").find('input').val('');
              $("#add-speech-form").find('textarea').val('');
              $("#add-speech-response").html('');
            }
            if("tab-view" == tabid){
              $("#speech-view-edit-delete-form").find('input').val('');
              $("#speech-view-edit-delete-form").find('textarea').val('');
              $('#views-edit-delete-keywords').tagsinput('removeAll');
              $(".btn-function-if-select").addClass('disabled');
              $("#update-speech-response").html('');
            }
      			$('#'+tabid).addClass('btn-primary');
      			$('#mobile-'+tabid).addClass('btn-primary');
      			$(tabx).addClass('show');
          }	
    		})

    		
    	});
      function resetTabs() {
        $('.btn-tab').removeClass('btn-primary');
        $('.content-card').removeClass('show');
      }
    </script>
    <!--end of tab plugin -->

   <!--show panel plugin -->
   <script>
   	  $(document).ready(function(){
   	  	$('.hidepanel').click(function(){
   	  		$('.sidebar.slide-panel').removeClass('slidein');
   	  	});
   	  	$('.showList').click(function(){
   	  		$('.sidebar.slide-panel').addClass('slidein');
   	  	});
   	  });	  
   </script>
   <!-- end show panel-->

   <script>
		$(document).ready(function () {
		    $('#bootstrapTagsInputForm')
		        .find('[name="keywords"]')
		            // Revalidate the cities field when it is changed
		            .change(function (e) {
		                $('#bootstrapTagsInputForm').formValidation('revalidateField', 'cities');
		            })
		            .end()
		        .find('[name="keywords-add"]')
		            // Revalidate the countries field when it is changed
		            .change(function (e) {
		                $('#bootstrapTagsInputForm').formValidation('revalidateField', 'countries');
		            })
		            .end()
		});
		</script>

    <!-- speech function -->
    <script>
        var base_url = '<?php echo json_encode(BASE_URL); ?>';
    </script>
    <script>
      $(document).ready(function () {
        
          $(".class-edit-func").keyup(function(){
            cehck_if_edit = true;
          });

          //get speech
          load_speech();

          $("#btn-add-speech-form").click(function(){
              var data = new FormData(document.getElementById('add-speech-form'));
              $(".fulloverlay").addClass('progress');
              $valid_data = true;
              $data_key="";

              if($("#author").val().trim() == ""){
                $valid_data = false;
                $data_key += "Author, ";
              }
              if($("#keywords-add").val().trim() == ""){
                $valid_data = false;
                $data_key += "Keyword/s, ";
              }
              if($("#datetimepickeradd").val().trim() == ""){
                $valid_data = false;
                $data_key += "Date, ";
              }
              if($("#speechInput").val().trim() == ""){
                $valid_data = false;
                $data_key += "Content, ";
              }

              if($valid_data)
              {
                cehck_if_edit = false;
                $.ajax({
                    url:"function/speech/add_speech",
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (data) {
                        $(".fulloverlay").removeClass('progress');
                        if(data)
                        {
                          alert_response('success','add-speech-response','Speech successfuly saved');
                          $("#add-speech-form").find('input').val('');
                          $("#add-speech-form").find('textarea').val('');
                          load_speech();
                        }
                    }
                });
              }else
              {
                $(".fulloverlay").removeClass('progress');
                alert_response('warning','add-speech-response','Error you need to fill '+$data_key+' to save this speech');
              }
          });
            
          $("#btn-edit-speech").click(function(){
              var data = new FormData(document.getElementById('speech-view-edit-delete-form'));
              $(".fulloverlay").addClass('progress');
              $valid_data = true;
              $data_key="";

              if($("#view-speech-author").val().trim() == ""){
                $valid_data = false;
                $data_key += "Author, ";
              }
              if($("#views-edit-delete-keywords").val().trim() == ""){
                $valid_data = false;
                $data_key += "Keyword/s, ";
              }
              if($("#datetimepicker1").val().trim() == ""){
                $valid_data = false;
                $data_key += "Date, ";
              }
              if($("#view-speech-content").val().trim() == ""){
                $valid_data = false;
                $data_key += "Content, ";
              }

              if($valid_data)
              {
                cehck_if_edit = false;
                $.ajax({
                    url:"function/speech/edit_speech",
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (data) {
                        $(".fulloverlay").removeClass('progress');
                        if(data)
                        {
                          alert_response('success','update-speech-response','Speech successfuly updated');
                        }
                        
                    }
                });
              }else
              {
                $(".fulloverlay").removeClass('progress');
                alert_response('warning','update-speech-response','Error you need to fill '+$data_key+' to save this speech');
              }
          });

          $("#btn-delete-speech").click(function(){
              var response = confirm('do you realy want to delete this speech?');

              if(response)
              {
                var data = new FormData(document.getElementById('speech-view-edit-delete-form'));
                $(".fulloverlay").addClass('progress');
                $.ajax({
                    url:"function/speech/delete_speech",
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (data) {
                        $(".fulloverlay").removeClass('progress');
                        if(data)
                        {
                          alert_response('success','update-speech-response','Speech successfuly deleted');
                          load_speech();
                        }
                        
                    }
                });
              }
          });

      });

      function edit_speech(speech_id)
      {
        if(cehck_if_edit)
          var con = confirm("Are you sure you want to leave this screen, any unsaved content will be lost.");
        else
        {
          var con = true;
        }

        if(con)
        {
          cehck_if_edit = false; 
          $(".fulloverlay").addClass('progress');
          $('#views-edit-delete-keywords').tagsinput('removeAll');
          $("#speech_list").find('li').each(function(){
            $(this).removeClass('active');
          });
          resetTabs();
          $.ajax({
              url:"function/speech/get_speech",
              type: 'POST',
              data:{'speech_id':speech_id},
              dataType:'json',
              success: function (data) {
                  var tabid = "tab-view";
                  $('#'+tabid).addClass('btn-primary');
                  $('#mobile-'+tabid).addClass('btn-primary');
                  $('#viewSpeech').addClass('show');
                  $(".btn-function-if-select").removeClass('disabled');
                  $(".speech-"+data[0].id).addClass('active');
                  $(".fulloverlay").removeClass('progress');
                  $("#view-speech-content").val(data[0].content);
                  $("#view-speech-author").val(data[0].author);
                  $("#datetimepicker1").val(data[0].date);
                  $("#view-speech-id").val(data[0].id);
                  var keywords = data[0].keywords.replace(/\+/g, ',');
                  $("#views-edit-delete-keywords").tagsinput('add',keywords);
              }
          });
        }
      }

      function load_speech()
      {
        $("#speech-view-edit-delete-form").find('input').val('');
        $("#speech-view-edit-delete-form").find('textarea').val('');
        $('#views-edit-delete-keywords').tagsinput('removeAll');
        $(".btn-function-if-select").addClass('disabled');
         $.ajax({
              url:"function/speech/get_speech",
              type: 'POST',
              dataType:'json',
              success: function (data) {
                  $("#speech_list").html('');
                  for(var i = 0; i < data.length; i++) {
                      appned_str = '<li class="speech-'+data[i].id+'" ><a href="javascript:edit_speech('+data[i].id+');">Speech '+data[i].id+'<i class="showbox fa fa-angle-right"></i></a></li>';
                      $("#speech_list").append(appned_str);
                  };
              }
          });
      }

      function alert_response(status,container,message)
      {

        var alert = '<div class="alert alert-'+status+' alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+message+'.</div>';

        $("#"+container).html(alert);
      }

    </script>
    <!-- end speech function -->
  </body>
</html>
