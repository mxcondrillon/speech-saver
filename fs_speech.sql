-- phpMyAdmin SQL Dump
-- version 4.0.10.15
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2017 at 02:39 AM
-- Server version: 5.1.68-community-log
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `BBBmrsg_fullstack`
--

-- --------------------------------------------------------

--
-- Table structure for table `fs_speech`
--

CREATE TABLE IF NOT EXISTS `fs_speech` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `author` varchar(45) NOT NULL,
  `keywords` text NOT NULL,
  `date` varchar(45) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `fs_speech`
--

INSERT INTO `fs_speech` (`id`, `author`, `keywords`, `date`, `content`) VALUES
(1, 'Mer pogi', 'Tags+yo', '01/17/2017', 'Pogi Mer'),
(3, '', 'Tags+Hello', '', 'Hello World'),
(4, 'mer', 'Tags', '01/31/2017', 'sample speech content mer'),
(5, 'Mers Biz', '', '01/31/2017', 'dddssasdd'),
(6, 'sdsddd', 'asadasd', '01/17/2017', 'asdasdsadsadsad'),
(7, 'sddsa', 'Tags', '01/10/2017', 'asddsdfgdf'),
(10, 'Someone 3', 'sadasd+asdsad+asdasd', '01/04/2017', 'Hello World'),
(11, 'dfgzfdgssfgfsgfsfggfgf', 'Tags', '02/20/2017', 'dasdasd'),
(17, '', '', '', 'fdfdfdf'),
(18, 'asdasd', 'sdsad+sdasd+sd', '02/02/2017', 'dsnhsdlsdnjms'),
(19, 'asdasddd', 'dskdjk', '02/06/2017', 'dddd'),
(20, 'TEST', '123', '02/08/2017', 'This is one');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
